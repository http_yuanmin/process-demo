package com.gjp.processdemo.config;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.zip.ZipInputStream;

/**
 * spring 容器初始化完毕后，执行自己的初始化逻辑
 */
@Component
@Slf4j
public class Initialized implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private IdentityService identityService;
    @Autowired
    private RepositoryService repositoryService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("开始初始化......");
        initUser();
        initGroup();
        initUserToGroup();
        initDeploy();
        log.info("初始化完毕。");
    }

    //初始化用户
    private void initUser() {
        log.info("初始化用户");
        //初始化流程所需要的身份信息
        User user1 = identityService.newUser("1");
        user1.setLastName("郭金鹏");
        user1.setPassword("123456");
        user1.setEmail("511015986@qq.com");

        User user2 = identityService.newUser("2");
        user2.setLastName("彭国杰");
        user2.setPassword("123456");
        user2.setEmail("12233431@qq.com");

        User user3 = identityService.newUser("3");
        user3.setLastName("徐秋伟");
        user3.setPassword("123456");
        user3.setEmail("84431293@qq.com");

        identityService.saveUser(user1);
        identityService.saveUser(user2);
        identityService.saveUser(user3);
    }

    //初始化用户组
    private void initGroup() {
        log.info("初始化用户组");
        Group groupEntity1 = identityService.newGroup("1");
        groupEntity1.setName("web开发");
        groupEntity1.setType("开发");
        identityService.saveGroup(groupEntity1);

        Group groupEntity2 = identityService.newGroup("2");
        groupEntity2.setName("人事部");
        groupEntity2.setType("行政");
        identityService.saveGroup(groupEntity2);

        Group groupEntity3 = identityService.newGroup("3");
        groupEntity3.setName("经理");
        groupEntity3.setType("高层");
        identityService.saveGroup(groupEntity3);
    }

    //初始化用户与组之间的关系
    private void initUserToGroup() {
        log.info("初始化用户与用户组之间的关系");
        identityService.createMembership("1", "1");
        identityService.createMembership("2", "2");
        identityService.createMembership("3", "3");
    }

    //部署已zip结尾的流程定义文件
    private void initDeploy() {
        log.info("部署zip流程定义文件");
        try {
            File processes = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "processes");
            for (File file : processes.listFiles()) {
                //如果文件已zip结尾，则直接部署
                if (file.getName().endsWith(".zip")){
                    repositoryService
                            .createDeployment()
                            .addZipInputStream(
                                    new ZipInputStream(
                                            new FileInputStream(file)))
                            .deploy();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
