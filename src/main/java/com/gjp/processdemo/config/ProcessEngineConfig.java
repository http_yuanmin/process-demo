package com.gjp.processdemo.config;

import org.activiti.engine.ProcessEngineConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProcessEngineConfig {

    /**
     * 修改 ProcessEngineConfiguration 配置，将默认的arial字体改为宋体
     * （好像也不能解决中文乱码问题，在生成流程图时，只是方便从这里获取这个值。。。）
     * @param processEngineConfiguration
     * @return
     */
    @Bean
    @ConditionalOnClass({ProcessEngineConfiguration.class})
    public ProcessEngineConfiguration processEngineConfiguration(ProcessEngineConfiguration processEngineConfiguration){
        processEngineConfiguration.setActivityFontName("宋体");
        processEngineConfiguration.setLabelFontName("宋体");
        processEngineConfiguration.setAnnotationFontName("宋体");
        return processEngineConfiguration;
    }
}
