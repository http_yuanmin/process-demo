package com.gjp.processdemo.controller;

import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    /**
     * 查询历史流程实例
     * @return
     */
    @RequestMapping("/history/process/list")
    public ModelAndView historyProcessList(){
        ModelAndView modelAndView = new ModelAndView("history-process-list");
        //查询所有流程实例
        List<HistoricProcessInstance> historicProcessInstanceList = historyService
                .createHistoricProcessInstanceQuery()
                .orderByProcessInstanceStartTime()
                .asc()
                .list();

        modelAndView.addObject("historicProcessInstanceList",historicProcessInstanceList);
        return modelAndView;
    }

    /**
     * 查询历史流程实例
     * @return
     */
    @RequestMapping("/history/process/{processInstanceId}")
    public ModelAndView historyProcess(
            @PathVariable("processInstanceId") String processInstanceId){
        ModelAndView modelAndView = new ModelAndView("history-process");

        //查询历史活动节点列表
        List<HistoricActivityInstance> historicActivityInstanceList = historyService
                .createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId)
                .orderByHistoricActivityInstanceStartTime()
                .desc()
                .list();

        //查询相关变量
        List<HistoricVariableInstance> historicVariableInstanceList = historyService
                .createHistoricVariableInstanceQuery()
                .processInstanceId(processInstanceId)
                .list();

        modelAndView.addObject("historicVariableInstanceList",historicVariableInstanceList);
        modelAndView.addObject("historicActivityInstanceList",historicActivityInstanceList);
        return modelAndView;
    }

}
