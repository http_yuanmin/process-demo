package com.gjp.processdemo.controller;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 查看用户 与 用户组
 */
@RestController
public class UserAndGroupController {

    @Autowired
    private IdentityService identityService;

    @RequestMapping("/user/{userId}")
    public User getUser(@PathVariable("userId") String userId){
        User user = identityService.createUserQuery().userId(userId).singleResult();
        return user;
    }

    @RequestMapping("/user/list")
    public List<User> userList(){
        List<User> userList = identityService.createUserQuery().list();
        return userList;
    }

    @RequestMapping("/group/{groupId}")
    public Group getGroup(@PathVariable("groupId") String groupId){
        Group group = identityService.createGroupQuery().groupId(groupId).singleResult();
        return group;
    }

    @RequestMapping("/group/list")
    public List<Group> groupList(){
        List<Group> groupList = identityService.createGroupQuery().list();
        return groupList;
    }
}