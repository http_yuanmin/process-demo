//package com.gjp.processdemo.controller;
//
//import com.gjp.processdemo.bean.FormData;
//import com.gjp.processdemo.bean.HistoryTaskData;
//import lombok.extern.slf4j.Slf4j;
//import org.activiti.engine.*;
//import org.activiti.engine.form.FormProperty;
//import org.activiti.engine.history.HistoricData;
//import org.activiti.engine.history.HistoricFormProperty;
//import org.activiti.engine.history.ProcessInstanceHistoryLog;
//import org.activiti.engine.impl.form.DateFormType;
//import org.activiti.engine.impl.form.StringFormType;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.*;
//
///**
// * 任务服务
// */
//@RestController
//@Slf4j
//public class TaskController {
//
//    @Autowired
//    private ProcessEngine processEngine;
//
////    /**
////     * 处理任务
////     * @param username
////     * @return
////     */
////    @RequestMapping("/handle/{username}/{taskId}")
////    public ModelAndView handleTask(
////            @PathVariable("username") String username
////            , @PathVariable("taskId") String taskId) {
////        ModelAndView modelAndView = new ModelAndView("task");
////        //获取该用户所拥有的任务列表
////        Task task = taskService
////                .createTaskQuery()
////                .taskAssignee(username)
////                .taskId(taskId)
////                .singleResult();
////        List<FormProperty> formPropertyList = formService
////                .getTaskFormData(task.getId())
////                .getFormProperties();
////        List<FormData> formDataList = handleFormPropertyList(formPropertyList);
////        Object renderedTaskForm = formService.getRenderedTaskForm(task.getId());
////        //查询历史表单数据
////        List<HistoryTaskData> historyTaskDataList = queryHistoricTaskFormData(task.getProcessInstanceId());
////        modelAndView.addObject("renderedTaskForm",renderedTaskForm);
////        modelAndView.addObject("task", task);
////        modelAndView.addObject("formDataList", formDataList);
////        modelAndView.addObject("historyTaskDataList", historyTaskDataList);
////        return modelAndView;
////    }
//
//    /**
//     * 根据流程实例ID，查询某个流程实例的历史任务记录
//     */
//    public List<HistoryTaskData> queryHistoricTaskFormData(String processInstanceId){
//        //查询 processInstanceHistoryLog 详细流程实例历史日志
//        ProcessInstanceHistoryLog processInstanceHistoryLog = processEngine
//                .getHistoryService()
//                .createProcessInstanceHistoryLogQuery(processInstanceId)
//                .includeFormProperties()//只包含表单属性
//                .singleResult();
//
//        //如果包含的Form属性为空，说明是刚启动的流程，没有历史Form表单属性
//        if (processInstanceHistoryLog.getHistoricData() == null || processInstanceHistoryLog.getHistoricData().size() < 1){
//            return null;
//        }
//
//        /**
//         * 结构  ,遍历包含的历史Form表单数据
//         *  id=1   1,2,3,4
//         *  id=2   8,8,8,8
//         */
//        Map<String,List<FormData>> TaskId_FormDataList = new HashMap<>();
//        for (HistoricData historicData : processInstanceHistoryLog.getHistoricData()) {
//            //由于知道实际类型，直接强转为 HistoricFormProperty
//            HistoricFormProperty historicFormProperty = (HistoricFormProperty) historicData;
//            //如果这个表单数据的所属实例ID 不等于传入的实例ID，则跳过
//            if (!historicFormProperty.getProcessInstanceId().equals(processInstanceId)){
//                continue;
//            }
//            //创建单个表单数据对象
//            FormData formData = new FormData();
//            formData.setId(historicFormProperty.getId());
//            formData.setName(historicFormProperty.getPropertyId());
//            formData.setValue(historicFormProperty.getPropertyValue());
//            //根据TaskId区分表单属性归属,先从map中，根据TaskId为key，查找所属的Form表单数据列表
//            List<FormData> existFormDataList = TaskId_FormDataList.get(historicFormProperty.getTaskId());
//            //如果为空，则说明是新环节的表单数据，新建后传入
//            if (existFormDataList == null || existFormDataList.size() < 1){
//                //说明是新
//                existFormDataList = new ArrayList<>();
//                existFormDataList.add(formData);
//                TaskId_FormDataList.put(
//                        historicFormProperty.getTaskId()
//                        ,existFormDataList
//                );
//            }else{//否则就是已有环节的表单数据，增量更新后，重新传入
//                existFormDataList.add(formData);
//                TaskId_FormDataList.put(
//                        historicFormProperty.getTaskId()
//                        ,existFormDataList
//                );
//            }
//        }
//
//        //拼装历史表单数据，根据TaskId区分环节，如果TaskId_FormDataList 有2个说明有2个环节已经提交完成
//        List<HistoryTaskData> historyTaskDataList = new ArrayList<>();
//        for (String taskIdKey : TaskId_FormDataList.keySet()) {
//            //组装为历史任务数据对象，并放入数组
//            HistoryTaskData historyTaskData = new HistoryTaskData();
//            historyTaskData.setTaskId(taskIdKey);
//            historyTaskData.setFormDataList(TaskId_FormDataList.get(taskIdKey));
//            historyTaskDataList.add(historyTaskData);
//        }
//        //排序
//        historyTaskDataList.sort(Comparator.naturalOrder());
//        return historyTaskDataList;
//    }
//
//    /**
//     * 处理List<FormProperty>
//     * @param formPropertyList
//     * @return
//     */
//    private List<FormData> handleFormPropertyList(List<FormProperty> formPropertyList){
//        List<FormData> formDataList = new ArrayList<>();
//        for (FormProperty formProperty : formPropertyList) {
//            if (DateFormType.class.isInstance(formProperty.getType())) {
//                FormData formData = new FormData();
//                formData.setId(formProperty.getId());
//                formData.setName(formProperty.getName());
//                formData.setType("date");
//                formData.setValue(formProperty.getValue());
//                formDataList.add(formData);
//            } else if (StringFormType.class.isInstance(formProperty.getType())) {
//                FormData formData = new FormData();
//                formData.setId(formProperty.getId());
//                formData.setName(formProperty.getName());
//                formData.setType("string");
//                formData.setValue(formProperty.getValue());
//                formDataList.add(formData);
//            }
//        }
//        return formDataList;
//    }
//
//    /**
//     * 完成任务
//     *
//     * @param taskId
//     * @param request
//     * @return
//     */
////    @RequestMapping("/complete")
////    public String completeTask(@RequestParam("taskId") String taskId, HttpServletRequest request) {
////        TaskFormData taskFormData = formService.getTaskFormData(taskId);
////        Map<String, String> variable = new HashMap<>();
////        //将用户提交的数据，动态添加到map中
////        for (FormProperty formProperty : taskFormData.getFormProperties()) {
////            if (formProperty.getType().equals(DateFormType.class)){
////                variable.put(formProperty.getId(), DateTime.parse(request.getParameter(formProperty.getId())).toString());
////            }else {
////                variable.put(formProperty.getId(), request.getParameter(formProperty.getId()));
////            }
////        }
////        formService.submitTaskFormData(taskId,variable);
//////        taskService.complete(taskId, variable);
////        return "提交成功!<a href='/task/list'>任务列表</a>";
////    }
//}
