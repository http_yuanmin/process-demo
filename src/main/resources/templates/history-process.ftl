<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>流程实例历史详情</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="/layui/layui.all.js"></script>
</head>
<body>
<table class="layui-table">
    <colgroup>
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
    </colgroup>
    <thead>
    <th>流程实例ID</th>
    <th>当前活动ID</th>
    <th>任务ID</th>
    <th>活动名称</th>
    <th>活动类型</th>
    <th>代理人</th>
    <th>开始时间</th>
    <th>结束时间</th>
    </thead>
    <tbody>
    <#list historicActivityInstanceList! as historicActivityInstance>
    <tr>
        <td>${ historicActivityInstance.processInstanceId! }</td>
        <td>${ historicActivityInstance.id! }</td>
        <td>${ historicActivityInstance.taskId! }</td>
        <td>${ historicActivityInstance.activityName! }</td>
        <td>${ historicActivityInstance.activityType! }</td>
        <td>${ historicActivityInstance.assignee! }</td>
        <td>${ historicActivityInstance.startTime!?string("yyyy-MM-dd HH:mm:ss") }</td>
        <#if historicActivityInstance.endTime??>
            <td>${ historicActivityInstance.endTime!?string("yyyy-MM-dd HH:mm:ss") }</td>
        <#else>
            <td>进行中</td>
        </#if>
    </tr>
    </#list>
    </tbody>
</table>
<br>
<h1>流程相关变量</h1>
<table class="layui-table">
    <colgroup>
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
    </colgroup>
    <thead>
    <th>流程实例ID</th>
    <th>任务ID</th>
    <th>变量名称</th>
    <th>变量类型</th>
    <th>变量值</th>
    <th>创建时间</th>
    <th>最后更新时间</th>
    </thead>
    <tbody>
        <#list historicVariableInstanceList! as historicVariableInstance>
        <tr>
            <td>${ historicVariableInstance.processInstanceId! }</td>
            <td>${ historicVariableInstance.taskId! }</td>
            <td>${ historicVariableInstance.variableName! }</td>
            <td>${ historicVariableInstance.variableTypeName! }</td>
            <td>${ historicVariableInstance.value! }</td>
            <td>${ historicVariableInstance.createTime!?string("yyyy-MM-dd HH:mm:ss") }</td>
            <td>${ historicVariableInstance.lastUpdatedTime!?string("yyyy-MM-dd HH:mm:ss") }</td>
        </tr>
        </#list>
    </tbody>
</table>
<script>
    layui.table.render();
    layui.form.render();
</script>
</body>
</html>