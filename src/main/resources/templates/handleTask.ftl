<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>处理任务</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="/layui/layui.all.js"></script>
</head>
<body>
<#-- 如果是动态表单，显示历史数据start -->
<#if historicVariableInstances??>
    <#list historicVariableInstances as variable>
        <table class="layui-table">
            <colgroup>
                <col width="150">
                <col width="200">
            </colgroup>
            <tr>
                <td>${variable.variableName!}</td>
                <td>${variable.value!}</td>
            </tr>
        </table>
    </#list>
</#if>
<#-- 如果是动态表单，显示历史数据end -->

<form action="/complete" class="layui-form" style="width: 50%;">
    <input type="hidden" name="taskId" value="${task.id!}">
    <#-- 如果是外置表单，直接输出表单html内容 -->
    <#if renderedTaskForm??>
        ${renderedTaskForm!}
    <#elseif formPropertyList??>
        <#list formPropertyList as formProperty>
            <table class="layui-table">
                <colgroup>
                    <col width="150">
                    <col width="200">
                </colgroup>
                <tr>
                    <td>${formProperty.name}：</td>
                    <td>
                        <#if formProperty.type.name=="date">
                            <input name="${formProperty.id}" id="date" class="layui-input" type="text" value="${formProperty.value!}">
                        <#elseif formProperty.type.name=="string">
                            <input name="${formProperty.id}" class="layui-input" type="text" value="${formProperty.value!}">
                        </#if>
                    </td>
                </tr>
            </table>
        </#list>
    </#if>
    <input type="submit" class="layui-btn" value="提交任务">
</form>
<br>
<button class="layui-btn layui-btn-primary" onclick="assignTo()">指派任务</button>
<script>
    layui.table.render();
    layui.form.render();
    //执行一个laydate实例
    layui.laydate.render({
        elem: '#date' //指定元素
    });

    /**
     * 指派任务到某个人
     * @returns {boolean}
     */
    function assignTo() {
        layui.layer.prompt({
            formType: 0,
            value: '',
            title: '请输入被指派人'
            // area: ['400px', '350px'] //自定义文本域宽高
        }, function(value, index, elem){
            // alert(value); //得到value
            var username = value;
            layui.$.post({
                url: "/assignTo"
                , data: {"taskId":"${task.id!}","assignName":username}
                , success:function (res) {
                    if (res == "ok"){
                        layui.layer.alert("任务以指派给【"+username+"】",{icon: 6});
                        setTimeout(function () {
                            window.location.href = "/myTask";
                        },2000);
                    }
                }
            })
            layer.close(index);
        });
        return false;
    }
</script>
</body>
</html>