<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>我的任务列表</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="/layui/layui.all.js"></script>
</head>
<body>
<table class="layui-table">
    <thead>
        <th>显示流程详情</th>
        <th>任务id</th>
        <th>任务名称</th>
        <th>任务描述</th>
        <th>任务创建时间</th>
        <th>操作</th>
    </thead>
    <tbody>
    <#list taskList! as task>
        <tr>
            <td>
                <button class="layui-btn" onclick="showImage('${ task.processInstanceId!"" }')">显示流程图</button>
            </td>
            <td>${ task.id! }</td>
            <td>${ task.name! }</td>
            <td>${ task.description! }</td>
            <td>${ task.createTime!?string('yyyy-MM-dd HH:mm:ss') }</td>
            <td>
                <#if task.assignee??>
                    <a class="layui-btn" href="/handle/${task.id!}">办理</a>
                <#else>
                    <a href="/claim/${task.id!}" class="layui-btn layui-btn-primary">接取</a>
                </#if>
            </td>
        </tr>
    </#list>
    </tbody>
</table>

<img id="processImage" width="50%">

<script>
    var $ = layui.$;
    /**
     * 显示流程执行详情
     * @param processInstanceId
     */
    function showImage(processInstanceId) {
        $.get({
            url: "/activeProcess/image/"+processInstanceId
            , success:function (res) {
                $("#processImage").attr("src",res);
            }
        });
    }
</script>
</body>
</html>