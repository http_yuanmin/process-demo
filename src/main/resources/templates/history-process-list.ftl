<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>流程实例历史记录</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="/layui/layui.all.js"></script>
</head>
<body>
<table class="layui-table">
    <colgroup>
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
        <col width="150">
    </colgroup>
    <thead>
    <th>流程实例ID</th>
    <th>流程定义ID</th>
    <th>流程定义名称</th>
    <th>开始时间</th>
    <th>结束时间</th>
    <th>流程持续时间</th>
    <th>版本</th>
    <th>操作</th>
    </thead>
    <#if historicProcessInstanceList??>
        <tbody>
        <#list historicProcessInstanceList! as historicProcessInstance>
            <tr>
                <td>${ historicProcessInstance.id! }</td>
                <td>${ historicProcessInstance.processDefinitionId! }</td>
                <td>${ historicProcessInstance.processDefinitionName! }</td>
                <td>${ historicProcessInstance.startTime?string("yyyy-MM-dd HH:mm:ss") }</td>
                <#if historicProcessInstance.endTime??>
                    <td>${ historicProcessInstance.endTime?string("yyyy-MM-dd HH:mm:ss") }</td>
                    <td>${ (historicProcessInstance.durationInMillis!/1000)?c }秒</td>
                <#else>
                    <td>进行中</td>
                    <td>-秒</td>
                </#if>
                <td>${ historicProcessInstance.processDefinitionVersion! }</td>
                <td><a href="/history/process/${historicProcessInstance.id!}" class="layui-btn layui-btn-primary">详情</a></td>
            </tr>
        </#list>
        </tbody>
    </#if>
</table>

<script>
    layui.table.render();
    layui.form.render();
</script>
</body>
</html>