<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>流程列表</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <script src="/layui/layui.all.js"></script>
</head>
<body>
<table class="layui-table">
    <thead>
        <tr>
            <th>流程图</th>
            <th>流程名称</th>
            <th>流程ID</th>
            <th>流程Key</th>
            <th>版本</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
    <#list processList! as process>
        <tr>
            <td><button class="layui-btn layui-btn-primary" onclick="showImage('${ process.id!"" }')">显示流程图</button></td>
            <td>${ process.name!"" }</td>
            <td>${ process.id!"" }</td>
            <td>${ process.key!"" }</td>
            <td>${ process.version!"" }</td>
            <td>
                <a href="/start/${process.key!}" class="layui-btn">启动</a>
            </td>
        </tr>
    </#list>
    </tbody>
</table>

<img id="processImage" width="50%">

<script>
    var $ = layui.$;

    /**
     * 显示流程图
     * @param deploymentId
     */
    function showImage(processDefinitionId) {
        $.get({
            url: "/process/image/"+processDefinitionId
            , success:function (res) {
                $("#processImage").attr("src",res);
            }
        });
    }
</script>
</body>
</html>