package com.gjp.processdemo;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.DelegationState;
import org.activiti.engine.task.Task;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 外指表单，测试LeaveApproval请假审批的流程
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MyLeaveTests {

    @Autowired
    private ProcessEngine processEngine;

    @Test
    public void contextLoads() {
        String currentUserId = "郭金鹏";
        //设置流程启动者
        processEngine.getIdentityService().setAuthenticatedUserId(currentUserId);
        ProcessInstance processInstance = processEngine.getRuntimeService()
                .startProcessInstanceByKey("LeaveApproval");
        Assert.assertEquals(currentUserId,processInstance.getStartUserId());

        //查询 currentUserId 的申请信息任务
        Task task = processEngine.getTaskService().createTaskQuery().taskCandidateOrAssigned(currentUserId).active().singleResult();
        Assert.assertEquals(currentUserId,task.getAssignee());
        Assert.assertEquals("申请信息",task.getName());
        Object renderedTaskForm = processEngine.getFormService().getRenderedTaskForm(task.getId());
        log.info("填写申请信息={}",renderedTaskForm);
        //提交申请信息
        Map<String,String> appInfoMap = new HashMap<>();
        appInfoMap.put("leaveType","事假");
        appInfoMap.put("startTime","2018-09-01");
        appInfoMap.put("endTime","2018-09-02");
        appInfoMap.put("reason","放假回家");
        processEngine.getFormService().submitTaskFormData(task.getId(),appInfoMap);

        //查询 彭国杰 的经理审批任务
        task = processEngine.getTaskService().createTaskQuery()
                .taskCandidateOrAssigned("彭国杰")
                .active()
                .singleResult();
        Assert.assertEquals("彭国杰",task.getAssignee());
        Assert.assertEquals("经理审批",task.getName());
        renderedTaskForm = processEngine.getFormService()
                .getRenderedTaskForm(task.getId());
        log.info("经理审批={}",renderedTaskForm);
        //提交经理审批
        appInfoMap = new HashMap<>();
        appInfoMap.put("managerApprovalType","同意");
        processEngine.getFormService().submitTaskFormData(task.getId(),appInfoMap);

        //查询 徐秋伟 的人事审批任务
        task = processEngine.getTaskService().createTaskQuery().taskCandidateOrAssigned("徐秋伟").active().singleResult();
        Assert.assertEquals("徐秋伟",task.getAssignee());
        Assert.assertEquals("人事审批",task.getName());
        renderedTaskForm = processEngine.getFormService().getRenderedTaskForm(task.getId());
        log.info("人事审批={}",renderedTaskForm);
        //提交人事审批
        appInfoMap = new HashMap<>();
        appInfoMap.put("hrApprovalType","驳回");
        appInfoMap.put("comment","不能和其他人一起连休，请修改休假时间");
        processEngine.getFormService().submitTaskFormData(task.getId(),appInfoMap);

        //任务被驳回到 调整申请 环节
        task = processEngine.getTaskService().createTaskQuery().taskCandidateOrAssigned(currentUserId).active().singleResult();
        Assert.assertEquals(currentUserId,task.getAssignee());
        Assert.assertEquals("调整申请",task.getName());
        renderedTaskForm = processEngine.getFormService().getRenderedTaskForm(task.getId());
        log.info("调整申请={}",renderedTaskForm);
        //提交 调整申请
        appInfoMap = new HashMap<>();
        appInfoMap.put("submitType","重新申请");
        processEngine.getFormService().submitTaskFormData(task.getId(),appInfoMap);

        //重新填写 申请信息
        task = processEngine.getTaskService().createTaskQuery().taskCandidateOrAssigned(currentUserId).active().singleResult();
        Assert.assertEquals(currentUserId,task.getAssignee());
        Assert.assertEquals("申请信息",task.getName());
        renderedTaskForm = processEngine.getFormService().getRenderedTaskForm(task.getId());
        log.info("填写申请信息={}",renderedTaskForm);
        //提交申请信息
        appInfoMap = new HashMap<>();
        appInfoMap.put("leaveType","事假");
        appInfoMap.put("startTime","2018-09-03");
        appInfoMap.put("endTime","2018-09-04");
        appInfoMap.put("reason","回去结婚");
        processEngine.getFormService().submitTaskFormData(task.getId(),appInfoMap);

        //查询 彭国杰 的经理审批任务
        task = processEngine.getTaskService().createTaskQuery().taskCandidateOrAssigned("彭国杰").active().singleResult();
        Assert.assertEquals("彭国杰",task.getAssignee());
        Assert.assertEquals("经理审批",task.getName());
        renderedTaskForm = processEngine.getFormService().getRenderedTaskForm(task.getId());
        log.info("经理审批={}",renderedTaskForm);
        //提交经理审批
        appInfoMap = new HashMap<>();
        appInfoMap.put("managerApprovalType","同意");
        processEngine.getFormService().submitTaskFormData(task.getId(),appInfoMap);

        //查询 徐秋伟 的人事审批任务
        task = processEngine.getTaskService().createTaskQuery()
                .taskCandidateOrAssigned("徐秋伟")
                .active()
                .singleResult();
        Assert.assertEquals("徐秋伟",task.getAssignee());
        Assert.assertEquals("人事审批",task.getName());
        renderedTaskForm = processEngine.getFormService()
                .getRenderedTaskForm(task.getId());
        log.info("人事审批={}",renderedTaskForm);

        //委派人事审批，委派给 卢杰
        processEngine.getTaskService().delegateTask(task.getId(),"卢杰");
        //查询 卢杰 的人事审批任务
        task = processEngine.getTaskService().createTaskQuery()
                .taskCandidateOrAssigned("卢杰")
                .taskDelegationState(DelegationState.PENDING)
                .active()
                .singleResult();
        Assert.assertEquals(DelegationState.PENDING,task.getDelegationState());
        Assert.assertEquals("卢杰",task.getAssignee());
        Assert.assertEquals("人事审批",task.getName());
        log.info("任务办理人={}，任务所属={}",task.getAssignee(),task.getOwner());
        renderedTaskForm = processEngine.getFormService().getRenderedTaskForm(task.getId());
        log.info("别人委派给卢杰的任务 = {}",renderedTaskForm);

        //卢杰 解决人事给他的任务
        processEngine.getTaskService().resolveTask(task.getId());

        //查询 徐秋伟 委派给 卢杰的人事审批任务是否完成
        task = processEngine.getTaskService().createTaskQuery()
                .taskCandidateOrAssigned("徐秋伟")
                .taskDelegationState(DelegationState.RESOLVED)
                .active()
                .singleResult();
        Assert.assertEquals(DelegationState.RESOLVED,task.getDelegationState());
        Assert.assertEquals("徐秋伟",task.getAssignee());
        Assert.assertEquals("人事审批",task.getName());
        renderedTaskForm = processEngine.getFormService()
                .getRenderedTaskForm(task.getId());
        log.info("人事审批={}",renderedTaskForm);

        //完成该任务
        appInfoMap = new HashMap<>();
        appInfoMap.put("hrApprovalType","同意");
        processEngine.getFormService().submitTaskFormData(task.getId(),appInfoMap);

        //查询 销假任务
        task = processEngine.getTaskService().createTaskQuery()
                .taskCandidateOrAssigned(currentUserId)
                .active()
                .singleResult();
        Assert.assertEquals(currentUserId,task.getAssignee());
        Assert.assertEquals("销假",task.getName());
        renderedTaskForm = processEngine.getFormService()
                .getRenderedTaskForm(task.getId());
        log.info("销假={}",renderedTaskForm);

        //提交销假任务，并结束流程
        appInfoMap = new HashMap<>();
        processEngine.getFormService().submitTaskFormData(task.getId(),appInfoMap);

        //流程已经结束
        String processInstanceId = processInstance.getId();
        processInstance = processEngine.getRuntimeService().createProcessInstanceQuery().processInstanceId(processInstance.getId()).singleResult();
        Assert.assertNull(processInstance);

        //查询历史任务
//        ProcessInstanceHistoryLog processInstanceHistoryLog = processEngine.getHistoryService()
//                .createProcessInstanceHistoryLogQuery(processInstanceId)
//                .includeFormProperties()
//                .singleResult();
//        List<HistoricData> historicDataList = processInstanceHistoryLog.getHistoricData();
//        Map<String,HistoricFormProperty> historicFormPropertyMap = new HashMap<>();
//        for (HistoricData historicData : historicDataList) {
//            HistoricFormProperty formProperty = (HistoricFormProperty) historicData;
//            HistoricFormProperty mapValue = historicFormPropertyMap.get(formProperty.getPropertyId());
//            if (Objects.isNull(mapValue)){
//                historicFormPropertyMap.put(formProperty.getPropertyId(),formProperty);
//            }else{
//                if (formProperty.getTime().compareTo(mapValue.getTime()) >= 0){
//                    historicFormPropertyMap.put(formProperty.getPropertyId(),formProperty);
//                }
//            }
//        }
        //查询历史变量
        List<HistoricVariableInstance> historicVariableInstanceList = processEngine.getHistoryService()
                .createHistoricVariableInstanceQuery()
                .processInstanceId(processInstanceId)
                .list();

        //打印最终数据
        for (HistoricVariableInstance historicVariableInstance : historicVariableInstanceList) {
            log.info("{}={}"
                    ,historicVariableInstance.getVariableName()
                    ,historicVariableInstance.getValue());
        }
    }

}
